#This program is incomplete. It runs but is not a full featured file browser. It just browses
import os #Known bugs attempting to open a file in a empty directory crashes program. Should be easy fix. 
import sys
import magic
import curses
fileTest = magic.Magic(mime=True); #Creates a object for determining file types
selectedFiles=[]; #This is the list of selected files see below
class fileSelect: #This is for storing a list information about selected Files
	def __init__(self, fileName, directory):
		self.fileName = fileName
		self.directory = directory
	fileName=""
	directory=""
	def __eq__(self, other): #Checks filename and directory
		if self.fileName==other.fileName and self.directory==other.directory:
			return True
		else:
			return False
def writeList(): #This function creates a list in the /tmp/ directory that other applications can use.
	global selectedFiles #It works, but isn't used yet.
	tempTxt=open("/tmp/selectedList","w") #Needs to somewhere store the pwd as well not added yet
	for i in selectedFiles:
		tempTxt.write(i.directory+"/"+i.fileName+"\n") #Wierd note python removes file when program closes automatically

def shorten(text, windowWidth): #This is used for the text preview method
        if(len(text)<windowWidth-2):
                return text
        else:
                return text[:windowWidth-2]+'$'

class fileInfo: #This is like fileselect but stores more information for displaying objects
	fileName=""
	isFolder=False
	isHidden=False
	Color=""
	isSelected=""
	text=""
	directory=""
	def sizeof_fmt(self, num, suffix='B'): #One function stollen from stack overflow
	    for unit in ['','Ki','Mi','Gi','Ti','Pi','Ei','Zi']: #by Fred Ciciera.
	        if abs(num) < 1024.0:
	            return "%3.1f%s%s" % (num, unit, suffix)
	        num /= 1024.0
	    return "%.1f%s%s" % (num, 'Yi', suffix)

	def formatCwd(self, windowWidth): #This function is meant to change the unformatted filename into the formatted text to display.
		if os.path.isdir(self.fileName): #This part is for formatting if the file is pointing towards a folder
			if(len(self.fileName)<windowWidth-1):
				self.text=(self.isSelected+self.fileName+(windowWidth-2-len(self.fileName))*' ');
			else: #if the text is longer then the screen
				self.text=((self.isSelected+self.fileName)[:windowWidth-2]+'$');
		else: #This part is for formatting the file if it is pointing towards a file not folder
			filesize=self.sizeof_fmt(os.stat(self.fileName).st_size)
			if(len(self.fileName+' '+filesize)<windowWidth-1): #Displays text if fits on screen
				self.text=(self.isSelected+self.fileName+(windowWidth-2-(len(self.fileName)+len(filesize)))*' '+filesize);
			else: #Shortens adds cash mark
				self.text=((self.isSelected+self.fileName+' '+filesize)[:windowWidth-2]+'$');
	def __eq__(self, other): #CHecks filename and directory same as fileinfo function
		if self.fileName==other.fileName and self.directory==other.directory:
			return True
		else:
			return False

def getOrderedCwd(oscwd): #adds files with . to the end and sorts everything by alphabetical order
	dList=os.listdir(oscwd); #OS Current work directory idk
	a = [];
	b = [];
	c = [];	#This orders by folders first files. Then hidden folders then hidden files.
	d = [];
	lf=False
	for i in range(0,len(dList)):
               	if dList[i] == "lost+found": #Blah blah moves lost+found to the end
               	        lf=True
               	else:
               	        if os.path.isdir(dList[i]):
               	                if dList[i][0]==".":
               	                        b.append(dList[i]);
               	                else:
               	                        a.append(dList[i]);
               	        else:
               	                if dList[i][0]==".":
               	                        d.append(dList[i]);
               	                else:
               	                        c.append(dList[i]);
	if lf:
		d.append("lost+found") #moves
	dList=sorted(a,key=str.lower)+sorted(c,key=str.lower)+sorted(b,key=str.lower)+sorted(d,key=str.lower)
	#This shit is there to sort the list by alphabetical order
	tempList=[];
	for i in range(0,len(dList)):	
		fInfo=fileInfo();
		if os.path.isdir(dList[i]):
			if dList[i][0]==".":
				#Hidden Directory
				fInfo.isFolder=True;
				fInfo.isHidden=True;
				fInfo.color=10;
			else:
				#Non Hidden Directory
				fInfo.isFolder=True;
				fInfo.isHidden=False;
				fInfo.color=28;

		else:
			if dList[i][0]==".":
				#Hidden File
				fInfo.isFolder=False;
				fInfo.isHidden=True;
				fInfo.color=84;
			else:
				#Non Hidden File
				fInfo.isFolder=False;
				fInfo.isHidden=False;
				fInfo.color=16;
		fInfo.fileName=dList[i];
		fInfo.directory=os.getcwd();

		tempList.append(fInfo);
	#This code checks if the stuff is selected choice
	global selectedFiles
	for i in selectedFiles:
		for j in tempList:
			if i==j:
				j.isSelected=" "
	return tempList;

class main():
	global selectedFiles
	stdscr = curses.initscr() #Start Cruses
	stdscr.keypad(True)
	stdscr.nodelay(True) #This is a workaround due to getch pausing the loop on the first run before being called. Wierd.
	curses.noecho()
	characterIn=''
	curses.start_color()
	curses.use_default_colors()
	curses.curs_set(0)
	for i in range(0, curses.COLORS): #This line were from stack overflow as well, because I didn't want to figure out how to initialize all the colors
		curses.init_pair(i + 1, i, -1); #and this two -Chief ten Brinke
	#This part is setting up the display windows and other junk before the main loop
	LeftWin = curses.newwin(curses.LINES-2,(curses.COLS-1)//2, 1,0)
	rightWin = curses.newwin(curses.LINES-2,(curses.COLS-1)//2, 1,((curses.COLS-1)//2))
	topWin = curses.newwin(1,curses.COLS+1,0,0)
	selectedFile=0 #Determines which file will be selected stores location integer
	windowWidth=(curses.COLS -1)//2
	doubleTap=False #Makes you have to doubleTap before you can loop this is only meant for directories larger then the window
	directoryList=getOrderedCwd(os.getcwd());
	def formatSelectWindow(LeftWin,RightWin, selectFile,windowWidth,dList): #This function actually rights the screens
		LeftWin.erase()
		drawImage=""
		for i in range(0,min(len(dList),curses.LINES-2)):
			dList[i].formatCwd(windowWidth); #Calls the format function to format the line
			if selectFile==i:
				RightWin.erase()
				LeftWin.addstr(i,0,dList[i].text,curses.A_REVERSE)
				if (os.path.isdir(dList[i].fileName) and os.access(dList[i].fileName,os.R_OK)): # this part displays the preview contents for directories in the second window. Checks if has access.
					os.chdir(dList[i].fileName) #Enters directory for preview
					dListRight=getOrderedCwd(os.getcwd());
					for j in range(0,min(len(dListRight),curses.LINES-2)):
						dListRight[j].formatCwd(windowWidth); #Calls the format function to format the line
						RightWin.addstr(j,0,dListRight[j].text,curses.color_pair(dListRight[j].color))
					os.chdir("..") #Leaves preview directory
                                elif os.path.isfile(dList[i].fileName) and os.access(dList[i].fileName,os.R_OK): #This part is for displaying text files
                                	if fileTest.from_file(dList[i].fileName).find('text/') != -1:
						tempTxt=open(dList[i].fileName,"r")
						text=tempTxt.read().split("\n")
                                        	tempTxt.close()
						for j in range(0,min(len(text)-1,curses.LINES-2)):
                                        		RightWin.addstr(j,0,shorten(text[j],windowWidth))

			else:
				if(dList[i].isSelected==" "):
					LeftWin.addstr(i,0,dList[i].text,curses.color_pair(12)) #This writes the left window
				else:
					LeftWin.addstr(i,0,dList[i].text,curses.color_pair(dList[i].color)) #This writes the left window

		LeftWin.refresh()
		RightWin.refresh()

	directoryList=getOrderedCwd(os.getcwd()); #here to fill the list when the application startx
	while characterIn!=24: #This means ctrl+x to quit 0-26 is ctrl + alphabet letter
		topWin.erase()
		if curses.LINES>4 and curses.COLS>4: #Makes sure window is big enough to run code current minimum size is 5x5
	
	
			topBar='TRIP Xplore '+os.getcwd() #This stuff is for making the top bar
			if len(topBar)>=curses.COLS-1:
				topBar=topBar[:curses.COLS]
			else:
				topBar+=(curses.COLS-len(topBar))*' '
	
	
			topWin.addstr(0,0,topBar,curses.A_STANDOUT)
			topWin.refresh()
			formatSelectWindow(LeftWin,rightWin,selectedFile,windowWidth,directoryList)
		characterIn = stdscr.getch()
		if characterIn != curses.KEY_UP: #This stuff is for the self.doubleTap
			doubleTap=False
		if characterIn == curses.KEY_RESIZE: #This statement updates the screen when the window size has changed
        	        fileTemp=directoryList[selectedFile]
			curses.LINES,curses.COLS=stdscr.getmaxyx() #This replaced the update rows columns function. It was buggy and didn't work
               		windowWidth=(curses.COLS -1)//2  #	This fucker ->curses.update_lines_cols() also this can run in python 2 as well
                	LeftWin.resize(curses.LINES - 1, (curses.COLS -1)//2)
                	rightWin.resize(curses.LINES - 1, (curses.COLS -1)//2)
                	rightWin.mvwin(1,(curses.COLS-1)//2)
                	topWin = curses.newwin(1,curses.COLS+1,0,0)
			if selectedFile>curses.LINES -1: #Checks if moving the order is necessary when resizing the window smaller
				if fileTemp!=directoryList[curses.LINES-3]: #Tries to keep file selector at the same location
					selectedFile=curses.LINES-3
				while fileTemp!=directoryList[curses.LINES-3]: #Tries to keep file selector at the same location
					directoryList.insert(0,directoryList.pop(len(directoryList)-1))
                	stdscr.nodelay(True)
		elif characterIn == curses.KEY_DOWN: #moves select down
			if  selectedFile<min(len(directoryList),curses.LINES-2)-1:
				selectedFile+=1
			elif len(directoryList)>curses.LINES-2: #this helps display directories that are too long
			#the elif condition is there to for when the list is smaller then display
				directoryList.append(directoryList.pop(0))
		elif characterIn == curses.KEY_UP and selectedFile>=0: #moves select up
			if selectedFile==0 and len(directoryList)>curses.LINES-2 and doubleTap:
				directoryList.insert(0,directoryList.pop(len(directoryList)-1))
				doubleTap=False
			if selectedFile==0 and len(directoryList)>curses.LINES-2:
				doubleTap=True
			elif selectedFile!=0:
				selectedFile-=1
		elif characterIn == curses.KEY_RIGHT and os.path.isdir(directoryList[selectedFile].fileName): #this changes directories .. cd
			if os.access(directoryList[selectedFile].fileName,os.R_OK):
				os.chdir(directoryList[selectedFile].fileName)
				directoryList=getOrderedCwd(os.getcwd());
				selectedFile=0
		elif characterIn == curses.KEY_LEFT: #goes back a directory cd .. pretty much
			os.chdir('..')
			directoryList=getOrderedCwd(os.getcwd());
			selectedFile=0
		elif characterIn == ord(' '): #File selection code
			if directoryList[selectedFile].isSelected=="":
				directoryList[selectedFile].isSelected=" "
				fileselect = fileSelect(directoryList[selectedFile].fileName,os.getcwd());
				selectedFiles.append(fileselect)
			else:
				directoryList[selectedFile].isSelected=""
				for i in selectedFiles:
					if i==directoryList[selectedFile] and i==directoryList[selectedFile]:
						selectedFiles.remove(i)
						directoryList[selectedFile].isSelected=""
			if  selectedFile<min(len(directoryList),curses.LINES-2)-1: #This part moves the stuff down after selected
				selectedFile+=1
			elif len(directoryList)>curses.LINES-2: #this helps display directories that are too long
			#the elif condition is there to for when the list is smaller then display
				directoryList.append(directoryList.pop(0))
			#writeList() # I imagine this function will be ran before implementing a command. I want to use external programs that are called by key codes set in a config file.
		#elif characterIn == ord(':'): # writelist createas a file that external programs can read.
		else:
			stdscr.nodelay(False)
	curses.nocbreak() #Exit curses
	stdscr.keypad(False)
	curses.echo()
	curses.endwin()


if __name__ == "__main__":
	#readList()
	main()
