xplore file browser incomplete use at your own risk no warrenty etc....
launch with: python xplore.py

Heres my general idea for things to implement or stuff I'll do when I update this eventually.

Todo list(not set in stone):
	-add pwd info to /tmp/selectFile
	-create config file:
		-Keyboard controls
		-Program launching and binding to a keycode, they can use the info in the /tmp/selectFile
			For example ^U would launch another program that would read the file names in /tmp/selectFile
			and use the pwd to copy and paste. Doing it this way would make the program a bit more flexible.
			This could also be hardcoded in, for moving files, since its simple, but still...
		-Default applications for opening files
	-Fix bug where program crashes trying to select a file in a empty directory
	-implement simple file operations preferably through external programs using the info /tmp/selectFile
	-The ability to open files.
	-File rename
	-Group File rename
	-Maybe tabs
	-idk

https://www.youtube.com/watch?v=gHtE7KySHa0